package com.example.murat.myapplication;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;


public class Favorites extends ListActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve preferences
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.PREFS), Context.MODE_PRIVATE);
        HashSet<String> favList = (HashSet)sharedPref.getStringSet("favs", null);

        if (favList != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, favList.toArray(new String[favList.size()]));
            getListView().setAdapter(adapter);
        }
        else{
            showToast(getString(R.string.NoFavoritesToast));
        }
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
    }

    /**
     * Called when clicking on an item from the list (to send a favorite)
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView tmp = (TextView) view;
        sendFavorite(tmp);
    }

    /**
     * Called when long clicking on an item from the list (in order to edit it)
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        showAlertDialog(view);

        return true;
    }

    /**
     * Shows an alert dialog to modify the favorite
     * @param view
     */
    public void showAlertDialog(View view) {
        final TextView textView = (TextView) view;
        AlertDialog.Builder saveLocationDialog = new AlertDialog.Builder(this);
        saveLocationDialog.setTitle(getString(R.string.FavoriteEditDialog));
        final EditText favoriteName = new EditText(this);
        favoriteName.setText(textView.getText().toString());
        saveLocationDialog.setView(favoriteName);


        saveLocationDialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onClickSave(favoriteName, textView);

            }
        });

        saveLocationDialog.setNegativeButton("Cancel", null);
        saveLocationDialog.show();
    }

    /**
     * Called when pressing save on the alert dialog
     * @param newName
     * @param clickedView
     */
    public void onClickSave(View newName, View clickedView) {
        TextView textView = (TextView) newName;
        TextView textViewClicked = (TextView) clickedView;
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.PREFS), Context.MODE_PRIVATE);
        HashSet<String> favList = (HashSet)sharedPref.getStringSet("favs", null);
        HashSet <String> newFavList = new HashSet<String>();
        for(String each: favList){
            newFavList.add(each);
        }
        newFavList.remove(textViewClicked.getText().toString());
        newFavList.add(textView.getText().toString());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet("favs", newFavList);

        // Save and refresh the UI
        if(editor.commit()){
            showToast(getString(R.string.FavEditedToast));
            finish();
            startActivity(getIntent());
        }

    }



    public void sendFavorite(TextView view) {
        String text = view.getText().toString();
        Intent sendIntent = new Intent(this, SendMessage.class);
        sendIntent.putExtra(getString(R.string.PreDefMessage), text);
        startActivity(sendIntent);
    }

    public void showToast(String msg){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }


}
