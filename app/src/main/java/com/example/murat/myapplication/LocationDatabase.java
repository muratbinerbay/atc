package com.example.murat.myapplication;

import android.provider.BaseColumns;

/**
 * Created by Murat on 22.5.2015.
 */
public final class LocationDatabase {

    // Default constructor
    public LocationDatabase(){}

    // Column and table names
    public static abstract class DatabaseEntry implements BaseColumns {
        public static final String TABLE_NAME = "SavedLocations";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_CUSTOMNAME = "customName";

    }
}
