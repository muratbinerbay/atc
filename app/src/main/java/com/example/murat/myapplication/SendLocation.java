package com.example.murat.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class SendLocation extends AppCompatActivity{

    private static double lat, lng;
    private static MyDatabaseHelper mDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_location);

        if(savedInstanceState != null){
            //restore location
            TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
            textView.setText(savedInstanceState.getString("currentLoc"));
        }
        else{
            //Set default msg
            TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
            textView.setText(getString(R.string.GPSWaitMessage));
        }
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        MyLocationListener locationListener = new MyLocationListener();
        //Register for updates on both network and GPS provider
        if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        //Get access to database
        mDbHelper = new MyDatabaseHelper(this);

    }

    // Udates the address information
    public void updateAddress(Location l){

        TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
        List<Address> myList = null;

        //Check if users has internet access in order to prevent the app from crashing
        if(hasInternetConnection()){
            Geocoder myLocation = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                myList = myLocation.getFromLocation(l.getLatitude(), l.getLongitude(), 1);
                lat = l.getLatitude();
                lng = l.getLongitude();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Print the whole address
            String address = "";
            for (int i = 0; i<= myList.get(0).getMaxAddressLineIndex(); i++){
                address +=  myList.get(0).getAddressLine(i) + " \n";
            }
            textView.setText(address);

            //Check if the address is in the database to show custom name
            if(isSavedLocation(address)){
                textView = (TextView) findViewById(R.id.SavedLocationTextView);
                textView.setText(getFromDatabase(address));
            }
            else{
                textView = (TextView) findViewById(R.id.SavedLocationTextView);
                textView.setText(getString(R.string.NonSavedLocation));
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Generates a message with your current location
     * @param view
     */
    public void onClickLocationSendButton(View view) {
        TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
        String text = textView.getText().toString();
        Intent sendIntent = new Intent(this, SendMessage.class);
        sendIntent.putExtra(getString(R.string.PreDefLocation), text);
        startActivity(sendIntent);
    }

    /**
     * Launches a MapActivity
     * @param view
     */
    public void onClickViewMap(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        double [] coordinates = new double[2];
        coordinates[0] = lat;
        coordinates[1] = lng;
        intent.putExtra(getString(R.string.Coordinates), coordinates);
        startActivity(intent);
    }

    /**
     * Called when clicking on the saved location text
     * @param view
     */
    public void onClickSavedLocation(View view) {
        TextView textView = (TextView) findViewById(R.id.SavedLocationTextView);
        //Check if location has already been customized
        if(textView.getText().toString() == getString(R.string.NonSavedLocation)){
            showAlertDialog();
        }
        else{
            textView = (TextView) findViewById(R.id.SavedLocationTextView);
            String text = textView.getText().toString();
            Intent sendIntent = new Intent(this, SendMessage.class);
            sendIntent.putExtra(getString(R.string.PreDefLocation), text);
            startActivity(sendIntent);

        }
    }

    /**
     * Shows an alert dialog to enter a new name
     */
    public void showAlertDialog(){
        AlertDialog.Builder saveLocationDialog = new AlertDialog.Builder(this);
        saveLocationDialog.setTitle(getString(R.string.New_Location_Dialog));
        final EditText locationName = new EditText(this);
        locationName.setHint(getString(R.string.Enter_Location_Name_TextBox));
        saveLocationDialog.setView(locationName);

        saveLocationDialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
                if (addToDatabase(textView.getText().toString(), locationName.getText().toString())) {
                    showToast(getString(R.string.Location_Added_Toast));
                    TextView textView2 = (TextView) findViewById(R.id.SavedLocationTextView);
                    textView2.setText(getFromDatabase(textView.getText().toString()));
                }


            }
        });
        saveLocationDialog.setNegativeButton("Cancel", null);
        saveLocationDialog.show();
    }

    /**
     * Inserts the custom name into the database
     * @param locationAdress
     * @param locationName
     * @return
     */
    public boolean addToDatabase(String locationAdress, String locationName){
        return mDbHelper.insertLocation(locationAdress, locationName);
    }

    /**
     * Fetches the custom name from the database
     * @param locationAddress
     * @return
     */
    public String getFromDatabase(String locationAddress){
        return mDbHelper.getLocation(locationAddress);
    }

    /**
     * Checks if the location already has a custom name
     * @param address
     * @return
     */
    public boolean isSavedLocation(String address){
        if(mDbHelper.getLocation(address) == "")
            return false;
        else
            return true;

    }


    private class MyLocationListener implements LocationListener  {

        // Listens for a location change
        public void onLocationChanged(Location location) {
            updateAddress(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };

    /**
     * Shows a toast with the message msg
     * @param msg
     */
    public void showToast(String msg){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        TextView textView = (TextView) findViewById(R.id.CurrentLocationTextView);
        savedInstanceState.putString("currentLoc", textView.getText().toString());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Checks for internet connection
     * @return true if the user has internet access else false
     */
    public boolean hasInternetConnection()
    {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected())
        {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected())
        {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected())
        {
            return true;
        }
        return false;
    }
}
