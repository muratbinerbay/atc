package com.example.murat.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Murat on 22.5.2015.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SavedLocations.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + LocationDatabase.DatabaseEntry.TABLE_NAME + " (" +
                    LocationDatabase.DatabaseEntry._ID + " INTEGER PRIMARY KEY," +
                    LocationDatabase.DatabaseEntry.COLUMN_NAME_CUSTOMNAME + TEXT_TYPE + COMMA_SEP +
                    LocationDatabase.DatabaseEntry.COLUMN_NAME_ADDRESS + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + LocationDatabase.DatabaseEntry.TABLE_NAME;


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        //Create the database (only called once when application is installed)
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Adds a new custom name for an addres in the database
     * @param locationAddress
     * @param locationName
     * @return
     */
    public boolean insertLocation(String locationAddress, String locationName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(LocationDatabase.DatabaseEntry.COLUMN_NAME_ADDRESS, locationAddress);
        contentValues.put(LocationDatabase.DatabaseEntry.COLUMN_NAME_CUSTOMNAME, locationName);
        db.insert(LocationDatabase.DatabaseEntry.TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Gets the location custom name from the address in the database
     * @param locationAddress
     * @return
     */
    public String getLocation(String locationAddress) {
        String result = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + LocationDatabase.DatabaseEntry.TABLE_NAME + " WHERE " + LocationDatabase.DatabaseEntry.COLUMN_NAME_ADDRESS + "=\"" + locationAddress + "\"", null);
        if(res.getCount() != 0){
            res.moveToFirst();
            result = res.getString(res.getColumnIndex(LocationDatabase.DatabaseEntry.COLUMN_NAME_CUSTOMNAME));
        }
        return result;


    }
}



