package com.example.murat.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;

public class SendMessage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (extras.getString(getString(R.string.PreDefMessage)) != null) {
                EditText editText = (EditText) findViewById(R.id.UserMessagEditText);
                editText.setText(extras.getString(getString(R.string.PreDefMessage)));
            }
            else if(extras.getString(getString(R.string.PreDefLocation)) != null) {
                EditText editText = (EditText) findViewById(R.id.UserMessagEditText);
                editText.setText("I'm at " + extras.getString(getString(R.string.PreDefLocation)));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to show options for sending text
     * Text value being the string on the EditText in activity
     * @param view
     */
    public void onClickSendMessage(View view) {
        TextView textView = (TextView) findViewById(R.id.UserMessagEditText);
        String text = textView.getText().toString();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    /**
     * Creates new favoritesList or adds to existing list if
     * list already exists. The list is stored in SharedPreferences.
     * showToast is called if commits are successfull.
     * @param view
     */
    public void onClickAddToFav(View view) {
        //get current string in editText
        EditText editText = (EditText) findViewById(R.id.UserMessagEditText);
        String newFav = editText.getText().toString();

        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.PREFS), Context.MODE_PRIVATE);
        HashSet <String> myFavs = (HashSet) sharedPref.getStringSet("favs", null);
        HashSet <String> newFavList = new HashSet<String>();
        if(myFavs != null){
            for(String each: myFavs){
                newFavList.add(each);
            }

            newFavList.add(newFav);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putStringSet("favs", newFavList);
            if(editor.commit()){
                showToast();
            }


        }
        else{
            myFavs = new HashSet<String>();
            myFavs.add(newFav);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putStringSet("favs", myFavs);
            if(editor.commit()){
                showToast();
            }
        }

    }

    public void showToast(){
        Context context = getApplicationContext();
        CharSequence text = getString(R.string.AddToFavs);
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void showToast(String msg){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();

    }
}
