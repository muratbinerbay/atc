package com.example.murat.myapplication;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when clicking on the send message button
     * @param view
     */
    public void onClickButton(View view) {

       Intent intent = new Intent(this, SendMessage.class);
        startActivity(intent);

    }

    /**
     * Called when clicking on the send location button
     * @param view
     */
    public void onClickLocationButton(View view) {
        Intent intent = new Intent(this, SendLocation.class);
        startActivity(intent);
    }

    /**
     * Called when clicking on the send picture button
     * @param view
     */
    public void onClickSendPicture(View view) {
        Intent intent = new Intent(this, SendPicture.class);
        startActivity(intent);
    }

    /**
     * Called when clicking on the Favorites button
     * @param view
     */
    public void onClickViewFavorites(View view) {
        Intent intent = new Intent(this, Favorites.class);
        startActivity(intent);
    }
}
